# tini

init for containers. [krallin/tini](https://github.com/krallin/tini)

* Read
  [source code](https://github.com/krallin/tini/blob/master/src/tini.c)
  for documentation.
* Debian: [tini](https://tracker.debian.org/pkg/tini)
* Software with similar fonctionality: catatonit